package com.juddit.juddit_main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/*
* this activity is where you can change your password and username
* */

public class ProfileSettingsActivity extends AppCompatActivity {
    private EditText username, password;
    private Button changeBtn;
    private DatabaseReference mDatabase;
    private ValueEventListener eventListener;
    private FirebaseAuth mAuth;
    private String uid;
    private ImageView toolbarImage;
    private TextView headerText;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_profilesettings);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        headerText = (TextView) findViewById(R.id.header_textView);
        headerText.setText(getResources().getString(R.string.header_settings));
        setSupportActionBar(myToolbar);
        toolbarImage = (ImageView) findViewById(R.id.header_imageButton);
        username = (EditText) findViewById(R.id.profile_setting_name_edit);
        password = (EditText) findViewById(R.id.profile_settings_password_edit);
        changeBtn = (Button) findViewById(R.id.profile_setting_change_btn);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

// Create a reference with an initial file path and name
        StorageReference pathReference = storageRef.child(uid);
        //Gets image for toolbar
        try {
            final File localFile = File.createTempFile("Images", "bmp");
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    toolbarImage.setImageBitmap(my_image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //Changes password and/or username
    public void changeButtonOnClick(View view){
        if(password.getText().toString().length() >= 6){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String newPassword = password.getText().toString();

            user.updatePassword(newPassword)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d("********", "User password updated.");
                            }
                        }
                    });
        }else if(password.getText().toString().length() < 6 && password.getText().toString().length() > 0){
            Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.pass_phrase), Toast.LENGTH_SHORT);
            toast.show();
        }
        if(username.getText().toString().length() >= 3){
            mDatabase.child("users").child(uid).child("username").setValue(username.getText().toString());

            mDatabase.child("users").child(uid).addValueEventListener(eventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String user = dataSnapshot.child("username").getValue().toString();
                    String email = dataSnapshot.child("email").getValue().toString();
                    mDatabase.child("score").child(user).removeValue();
                    mDatabase.child("score").child(username.getText().toString()).setValue(email);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }else if(username.getText().toString().length() < 3 && username.getText().toString().length() > 0){
            Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.user_phrase), Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    public void createTopicRoomOnClick(View view) {
        Intent myIntent = new Intent(ProfileSettingsActivity.this, TopicRoomActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ProfileSettingsActivity.this.startActivity(myIntent);
    }
    public void createMyProfileOnClick(View view){
        Intent myIntent = new Intent(ProfileSettingsActivity.this, MyProfileActivity.class);
        ProfileSettingsActivity.this.startActivity(myIntent);
    }

    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
    }
}