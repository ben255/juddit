package com.juddit.juddit_main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/*
* Topic room activity loads the data from topicroom in the database, then displays it in the ListView
* It also sends the clicked room to the subview fragment.
* */
public class TopicRoomActivity extends AppCompatActivity {

    final static String english = "english";
    final static String swedish = "swedish";
    private String language = "";
    private ListView listView;
    private ArrayList<String> data;
    private ArrayList<String> dataEnglish;
    private DatabaseReference mDatabase;
    private ValueEventListener eventListener;
    static final String ROOM = "topicroom";
    private ImageView toolbarImage;
    private FirebaseAuth mAuth;
    private TextView headerText;
    private String uid;
    private Dialog settingsDialog;
    private ImageView imageView;
    private AnimationDrawable animationDrawable;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_room);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        toolbarImage = (ImageView) findViewById(R.id.header_imageButton);
        headerText = (TextView) findViewById(R.id.header_textView);
        headerText.setText(getResources().getString(R.string.header_room));

        listView = (ListView) findViewById(R.id.topic_room_listview);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        dataEnglish = new ArrayList<String>();

        //checks if the language of the device is swedish then choose that language or english, can add more easy
        if(Locale.getDefault().getDisplayLanguage().equals("svenska"))
            language = swedish;
        else
            language = english;

        settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
        imageView = (ImageView) settingsDialog.findViewById(R.id.animationImage);
        animationDrawable = (AnimationDrawable) imageView.getDrawable();
        settingsDialog.show();
        animationDrawable.start();

        updateList();


        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object obj = listView.getItemAtPosition(position);
                Intent myIntent = new Intent(TopicRoomActivity.this, CreateTopicSubFragmentViewer.class);
                //send the picked room to the next activity
                myIntent.putExtra("room", dataEnglish.get(position));
                TopicRoomActivity.this.startActivity(myIntent);

            }
        });

        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        // Create a reference with an initial file path and name
        StorageReference pathReference = storageRef.child(uid);

        try {
            //loads the image file to the toobarImage
            final File localFile = File.createTempFile("Images", "bmp");
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    toolbarImage.setImageBitmap(my_image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onResume(){
        super.onResume();
        //restarts the data when coming back to the view
        data = new ArrayList<String>();
        dataEnglish = new ArrayList<String>();
        updateList();
    }


    private void updateList(){
        // My top posts by number of stars
        //checks the queries in ROOM
        mDatabase.child(ROOM).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //the loading animation

                readData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                // ...
            }
        });
    }

    private void readData(DataSnapshot dataSnapshot){
        data = new ArrayList<String>();
        dataEnglish = new ArrayList<String>();
        //iterates the snapshot of all the queries in the topicroom query
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            //data is displayed on the listview depending on language
            data.add(postSnapshot.child(language).getValue().toString());
            //dataenglish is for the server to know what key to write and should always be english
            dataEnglish.add(postSnapshot.child(english).getValue().toString());
        }

        ListAdapter listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, data);
        listView.setAdapter(listAdapter);
        settingsDialog.dismiss();
    }

    public void createMyProfileOnClick(View view){
        Intent myIntent = new Intent(TopicRoomActivity.this, MyProfileActivity.class);
        TopicRoomActivity.this.startActivity(myIntent);
    }
    public void createTopicRoomOnClick(View view) {
    }

    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
        if(settingsDialog != null)
            settingsDialog.dismiss();
    }
}
