package com.juddit.juddit_main;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

/*
* This fragment loads the specific topics for a specific room then displays them
* */

public class TopicSubFragment extends Fragment {

    private ArrayList<String> listData;
    private ArrayList<String> listId;

    private Button createBtn;
    private ListView topicList;
    private String room;

    private TopicSubClick callback;
    private DatabaseReference mDatabase;
    private  ValueEventListener eventListener;

    private Dialog settingsDialog;
    private ImageView imageView;
    private AnimationDrawable animationDrawable;

    public void setTopicSub(Activity activity) {
        callback = (TopicSubClick) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData = new ArrayList<String>();
        listId = new ArrayList<String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle = this.getArguments();
        room = bundle.getString("room");

        return inflater.inflate(R.layout.fragment_topic_sub, container, false);
    }
    @Override
    public void onStart(){
        super.onStart();
        createBtn = (Button) getActivity().findViewById(R.id.topic_sub_btn);
        topicList = (ListView) getActivity().findViewById(R.id.topic_sub_list);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        settingsDialog = new Dialog(getContext());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
        imageView = (ImageView) settingsDialog.findViewById(R.id.animationImage);
        animationDrawable = (AnimationDrawable) imageView.getDrawable();
        settingsDialog.show();
        animationDrawable.start();

        //loads the topicsub and the specific room topics
        mDatabase.child("topicsub").child(room).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                readData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                // ...
            }
        });


        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sends back that the fragment button was clicked
                callback.subCreateClicked();
            }
        });

        topicList.setClickable(true);
        topicList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //sends back that a listitem was clicked
                callback.subListClicked(listId.get(position));

            }
        });
    }

    private void readData(DataSnapshot dataSnapshot){
        listData.clear();
        listId.clear();
        try{
            //iterates the snapshot of the quary
            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                listId.add(postSnapshot.getKey());
                listData.add(postSnapshot.child("title").getValue().toString());
            }
            //reverse the data so we get newest first
            Collections.reverse(listData);
            Collections.reverse(listId);
            ListAdapter listAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1, listData);
            topicList.setAdapter(listAdapter);
            settingsDialog.hide();
        }catch(NullPointerException e){

        }
    }

    public interface TopicSubClick{
       void subCreateClicked();
       void subListClicked(String topicId);

    }
    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
        if(settingsDialog != null)
            settingsDialog.dismiss();
    }
}
