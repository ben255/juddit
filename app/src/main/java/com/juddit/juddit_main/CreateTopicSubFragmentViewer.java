package com.juddit.juddit_main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/*
* Handles the SubViewFragment and TopicSubFragment and that clicks that occures in those fragments
* */

public class CreateTopicSubFragmentViewer extends AppCompatActivity implements TopicSubFragment.TopicSubClick, SubViewFragment.SubViewClick {

    private FragmentManager fm;
    private FragmentTransaction ft;
    private String topicId = "";

    private ImageView toolbarImage;
    private FirebaseAuth mAuth;
    private String uid;
    private TextView headerText;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_topic_sub_fragment_viewer);

        TopicSubFragment topicSub = new TopicSubFragment();
        Bundle myBundle = new Bundle();
        myBundle.putString("room", getIntent().getExtras().getString("room"));
        topicSub.setArguments(myBundle);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        toolbarImage = (ImageView) findViewById(R.id.header_imageButton);
        headerText = (TextView) findViewById(R.id.header_textView);
        headerText.setText(getIntent().getExtras().getString("room"));

        fm = this.getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.add(R.id.topic_sub_layout, topicSub);
        ft.commit();

        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

// Create a reference with an initial file path and name
        StorageReference pathReference = storageRef.child(uid);

        try {
            final File localFile = File.createTempFile("Images", "bmp");
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    toolbarImage.setImageBitmap(my_image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof TopicSubFragment) {
            TopicSubFragment headlinesFragment = (TopicSubFragment) fragment;
            headlinesFragment.setTopicSub(this);
        }
        if (fragment instanceof SubViewFragment) {
            SubViewFragment headlinesFragment = (SubViewFragment) fragment;
            headlinesFragment.setSubView(this);
        }
    }

    public void subCreateClicked(){
        Intent myIntent = new Intent(CreateTopicSubFragmentViewer.this, CreateTopic.class);
        myIntent.putExtra("room", getIntent().getExtras().getString("room"));
        CreateTopicSubFragmentViewer.this.startActivity(myIntent);

    }
    //switches fragment
    public void subListClicked(String topicId){
        SubViewFragment subView = new SubViewFragment();
        Bundle myBundle = new Bundle();
        this.topicId = topicId;
        myBundle.putString("topicid", topicId);
        myBundle.putString("room", getIntent().getExtras().getString("room"));
        subView.setArguments(myBundle);

        fm = this.getSupportFragmentManager();
        ft = fm.beginTransaction();

        ft.replace(R.id.topic_sub_layout, subView);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }
    public void subCommentClicked(){
        Intent myIntent = new Intent(CreateTopicSubFragmentViewer.this, CreateComment.class);
        myIntent.putExtra("topicid", topicId);
        CreateTopicSubFragmentViewer.this.startActivity(myIntent);

    }

    @Override
    public void listViewClicked(CommentData data) {
        Intent myIntent = new Intent(CreateTopicSubFragmentViewer.this, OtherProfileActivity.class);
        myIntent.putExtra("uid", data.uid);
        CreateTopicSubFragmentViewer.this.startActivity(myIntent);
    }
    public void createMyProfileOnClick(View view){
        Intent myIntent = new Intent(CreateTopicSubFragmentViewer.this, MyProfileActivity.class);
        CreateTopicSubFragmentViewer.this.startActivity(myIntent);
    }
    public void createTopicRoomOnClick(View view){
        Intent myIntent = new Intent(CreateTopicSubFragmentViewer.this, TopicRoomActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        CreateTopicSubFragmentViewer.this.startActivity(myIntent);

    }
}
