package com.juddit.juddit_main;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/*
* Creates the topic in the appropriet location in the database
* */

public class CreateComment extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ValueEventListener eventListener;

    private String username;

    private EditText editCommentText;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_comment);
        editCommentText = (EditText) findViewById(R.id.create_comment_edit_text);
        mAuth = FirebaseAuth.getInstance();
        mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("users").child(mAuth.getUid()).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                username = dataSnapshot.child("username").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

    }

    public void commentPostButtonClicked(View view){

        CommentData data = new CommentData(
                editCommentText.getText().toString(),
                mAuth.getUid(),
                username
        );
        mDatabase = FirebaseDatabase.getInstance().getReference();
        String key = mDatabase.child("comment").child(getIntent().getExtras().getString("topicid")).push().getKey();
        mDatabase.child("comment").child(getIntent().getExtras().getString("topicid")).child(key).setValue(data);
        finish();
    }

    public void commentCancleButtonClicked(View view){
        finish();
    }

    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
    }

}
