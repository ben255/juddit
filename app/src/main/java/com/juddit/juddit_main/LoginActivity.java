package com.juddit.juddit_main;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

/*
LoginActivity checks the email and password then checks if the server authorizes the details
 */

public class LoginActivity extends AppCompatActivity{

    private EditText userName, pass;
    private FirebaseAuth mAuth;

    private Dialog settingsDialog;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_login);


        userName = (EditText) findViewById(R.id.login_username_text);
        pass = (EditText) findViewById(R.id.login_password_text);
        mAuth = FirebaseAuth.getInstance();

        settingsDialog = new Dialog(this);


    }

    public void loginButtonClicked(View view){
        if(!userName.getText().toString().isEmpty() && !pass.getText().toString().isEmpty()) {


                mAuth.signInWithEmailAndPassword(userName.getText().toString(), pass.getText().toString())
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    //the loading animation
                                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                                    ImageView imageView = (ImageView) settingsDialog.findViewById(R.id.animationImage);
                                    AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
                                    animationDrawable.start();
                                    settingsDialog.show();
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();


                                    loginSucessfull(user);


                                } else {
                                    //Handle invalid password or email
                                    Toast toast = Toast.makeText(getApplicationContext(),R.string.invalid_name, Toast.LENGTH_SHORT);
                                    toast.show();

                                }
                            }
                        });
            }
    }
    private void loginSucessfull(FirebaseUser user){
        Intent myIntent = new Intent(LoginActivity.this, TopicRoomActivity.class);
        myIntent.putExtra("mauth", user);
        LoginActivity.this.startActivity(myIntent);
        finish();
    }
    public void createButtonClicked(View view){
        Intent myIntent = new Intent(LoginActivity.this, CreateAccountActivity.class);
        LoginActivity.this.startActivity(myIntent);
    }

    @Override
    public void onStop(){
        super.onStop();
        if(settingsDialog != null)
            settingsDialog.dismiss();
    }


}
