package com.juddit.juddit_main;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

public class ChoseDateActivity extends AppCompatActivity {

    DatePicker datePicker;

    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_choose_date);

        datePicker = (DatePicker) findViewById(R.id.choose_date_picker);
    }


    public void pickClick(View view){

        //sends data to the widget
        SharedPreferences sp = getSharedPreferences("dates", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("year", datePicker.getYear());
        editor.putInt("month", datePicker.getMonth());
        editor.putInt("day", datePicker.getDayOfMonth());
        editor.commit();


        //updates the widget
        Intent intent = new Intent(this, AppWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
// Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
// since it seems the onUpdate() is only fired on that:
        int[] ids = AppWidgetManager.getInstance(getApplication())
                .getAppWidgetIds(new ComponentName(getApplication(), AppWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);

        finish();

    }

    public void cancleClick(View view){
        finish();
    }
}
