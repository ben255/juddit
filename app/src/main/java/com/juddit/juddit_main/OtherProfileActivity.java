package com.juddit.juddit_main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

/*
* This is where the user you chose to click on is displayed with username and email. also the image the user has chosen
* */

public class OtherProfileActivity extends AppCompatActivity {
    private TextView username, email;

    private DatabaseReference mDatabase;
    private ValueEventListener eventListener;

    private ImageView toolbarImage, otherImage;
    private FirebaseAuth mAuth;
    private String uid;
    private String otherUid;
    private TextView headerText;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_otherprofile);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        toolbarImage = (ImageView) findViewById(R.id.header_imageButton);
        otherImage = (ImageView) findViewById(R.id.other_image_view);
        headerText = (TextView) findViewById(R.id.header_textView);

        username = (TextView) findViewById(R.id.other_username_edittext);
        email = (TextView) findViewById(R.id.other_email_edittext);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("users").child(getIntent().getExtras().getString("uid")).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                loadData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mAuth = FirebaseAuth.getInstance();
        uid = mAuth.getUid();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

// Create a reference with an initial file path and name
        StorageReference pathReference = storageRef.child(uid);

        try {
            final File localFile = File.createTempFile("Images", "bmp");
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    toolbarImage.setImageBitmap(my_image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadData(DataSnapshot dataSnapshot){
        username.setText(dataSnapshot.child("username").getValue().toString());
        email.setText(dataSnapshot.child("email").getValue().toString());
        headerText.setText(dataSnapshot.child("username").getValue().toString() + " " + getResources().getString(R.string.header_profile));

        mDatabase.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                loadUid(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void createMyProfileOnClick(View view){
        Intent myIntent = new Intent(OtherProfileActivity.this, MyProfileActivity.class);
        OtherProfileActivity.this.startActivity(myIntent);
    }
    public void createTopicRoomOnClick(View view) {
        Intent myIntent = new Intent(OtherProfileActivity.this, TopicRoomActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        OtherProfileActivity.this.startActivity(myIntent);
    }
    public void loadUid(DataSnapshot dataSnapshot){
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            if(postSnapshot.child("username").getValue().toString().equalsIgnoreCase(username.getText().toString())) {
                otherUid = postSnapshot.getKey();

                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();


// Create a reference with an initial file path and name


                StorageReference pathReference = storageRef.child(otherUid);

                try {
                    final File localFile = File.createTempFile("Images", "bmp");
                    pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                            otherImage.setImageBitmap(my_image);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
    }
}