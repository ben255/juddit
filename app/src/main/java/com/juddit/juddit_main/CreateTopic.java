package com.juddit.juddit_main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/*
* Creates the topic in the approproete location
* */
public class CreateTopic extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private EditText editTitle, editTopicText;
    private static final String SUB = "topicsub";

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_topic);
        editTitle = (EditText) findViewById(R.id.create_topic_edit_title_text);
        editTopicText = (EditText) findViewById(R.id.create_topic_edit_topic_text);
    }

    public void createTopicOnClick(View view){
        mAuth = FirebaseAuth.getInstance();
        mAuth.getCurrentUser();
        TopicData data = new TopicData(
                getIntent().getExtras().getString("room"),
                editTitle.getText().toString(),
                editTopicText.getText().toString(),
                mAuth.getUid()
        );
        mDatabase = FirebaseDatabase.getInstance().getReference();
        String key = mDatabase.child(SUB).child(getIntent().getExtras().getString("room")).push().getKey();
        mDatabase.child(SUB).child(getIntent().getExtras().getString("room")).child(key).setValue(data);
        finish();
    }
    public void cancleTopicOnClick(View view){
        finish();
    }

}
