package com.juddit.juddit_main;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;



import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/*
* creates accounts, have to check if email is appropriate, if user exists etc. It will also handle any of the exceptions that happen
* and will respon with a toast if there is something wrong with creating the account.
* */

public class CreateAccountActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    ValueEventListener eventListener;

    Button createBtn;
    EditText userText, passText, emailText;

    static String SCORE = "score";
    static String USERS = "users";
    boolean userExist = false;

    private Dialog settingsDialog;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_account);

        createBtn = (Button) findViewById(R.id.create_account_create_btn);
        userText = (EditText) findViewById(R.id.create_account_username_text);
        passText = (EditText) findViewById(R.id.create_account_password_text);
        emailText = (EditText) findViewById(R.id.create_account_email_text);

        settingsDialog = new Dialog(this);

    }

    public void CreateAccountOnClick(View view){

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("users").addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userExist = checkUser(dataSnapshot);

                if(!emailText.getText().toString().equals("") && !passText.getText().toString().equals(""))
                    if (emailText.getText().toString().contains("@student.ju.se")) {
                        if(!userExist) {

                            //the loading animation
                            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                            ImageView imageView = (ImageView) settingsDialog.findViewById(R.id.animationImage);
                            AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
                            animationDrawable.start();
                            settingsDialog.show();

                            mAuth.createUserWithEmailAndPassword(emailText.getText().toString(), passText.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        mAuth.getCurrentUser();
                                        mDatabase.child(USERS).child(mAuth.getCurrentUser().getUid()).child("username").setValue(userText.getText().toString());
                                        mDatabase.child(USERS).child(mAuth.getCurrentUser().getUid()).child("email").setValue(emailText.getText().toString());
                                        mDatabase.child(SCORE).child(userText.getText().toString()).setValue(emailText.getText().toString());
                                        finish();
                                    } else {
                                        try {
                                            throw task.getException();
                                        } catch (FirebaseAuthWeakPasswordException e) {
                                            Toast toast = Toast.makeText(getApplicationContext(), R.string.pass_phrase, Toast.LENGTH_SHORT);
                                            toast.show();
                                        } catch (FirebaseAuthUserCollisionException e) {
                                            Toast toast = Toast.makeText(getApplicationContext(), R.string.email_exist, Toast.LENGTH_SHORT);
                                            toast.show();
                                        } catch (Exception e) {
                                            Toast toast = Toast.makeText(getApplicationContext(), R.string.valid_email, Toast.LENGTH_SHORT);
                                            toast.show();
                                        }
                                    }
                                }
                            });
                        }
                        else{
                            Toast toast = Toast.makeText(getApplicationContext(), R.string.username_exist, Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                    else{
                        Toast toast = Toast.makeText(getApplicationContext(), R.string.juMail_name, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                else{
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.email_pass_empty, Toast.LENGTH_SHORT);
                    toast.show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    public void CancelAccountOnClick(View view){
        finish();
    }

    public boolean checkUser(DataSnapshot dataSnapshot){
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            if(postSnapshot.child("username").getValue().toString().equalsIgnoreCase(userText.getText().toString())){
                return true;

            }

        }
        return false;
    }
    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
        if(settingsDialog != null)
            settingsDialog.dismiss();
    }
}
