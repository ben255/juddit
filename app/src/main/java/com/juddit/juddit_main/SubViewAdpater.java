package com.juddit.juddit_main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class SubViewAdpater extends ArrayAdapter<CommentData>  implements View.OnClickListener{
    private ArrayList<CommentData> dataSet;
    private Context mContext;

    private TextView tView;
    private EditText eView;


    private DatabaseReference mDatabase;


    public SubViewAdpater(ArrayList<CommentData> data, Context context){
        super(context, R.layout.row_sub_view_item, data);
        this.dataSet = data;
        this.mContext = context;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        CommentData data = this.getItem(position);
        final View result;



        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.row_sub_view_item, parent, false);

            tView = (TextView) convertView.findViewById(R.id.row_sub_view_text);
            eView = (EditText) convertView.findViewById(R.id.row_sub_view_edit_text);
            eView.setFocusable(false);

            eView.setText(data.getText());
            tView.setText(data.getUsername());


        }
        return convertView;
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}
