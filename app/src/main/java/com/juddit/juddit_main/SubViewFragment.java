package com.juddit.juddit_main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/*
This fragment displays the topic subject text and who wrote it. It also has all the comments in a listview with a custom adapter.
Every comments gets a unique id, with the text and who wrote it saved.
* */

public class SubViewFragment extends Fragment {
    private SubViewClick callback;
    private TextView userName;
    private EditText userText;
    private Button commentBtn;
    private ListView listView;
    private ArrayList<CommentData> listData;
    private DatabaseReference mDatabase;
    private ValueEventListener eventListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listData = new ArrayList<CommentData>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sub_view, container, false);
    }
    public void setSubView(Activity activity) {
        callback = (SubViewFragment.SubViewClick) activity;
    }

    @Override
    public void onStart(){
        super.onStart();
        listData.clear();
        userName = (TextView) getActivity().findViewById(R.id.sub_view_name);
        userText = (EditText) getActivity().findViewById(R.id.sub_view_edit_text);
        commentBtn = (Button) getActivity().findViewById(R.id.sub_view_btn);
        listView = (ListView) getActivity().findViewById(R.id.sub_view_list_view);


        mDatabase = FirebaseDatabase.getInstance().getReference();

        Bundle bundle = this.getArguments();
        //loads the topic where the id and room is sent from the previous fragment
        mDatabase.child("topicsub").child(bundle.getString("room")).child(bundle.getString("topicid")).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                loadTopic(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("READINGFAILED", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        });
        //load the comments from a comment query, with the topic id key
        mDatabase.child("comment").child(bundle.getString("topicid")).addValueEventListener(eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                loadComments(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("READINGFAILED", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //tells the activty that a listitem was clicked and sennds back what item
                callback.listViewClicked(listData.get(position));
            }
        });

        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.subCommentClicked();
            }
        });

    }





    public void loadTopic(DataSnapshot dataSnapshot){
        userText.setText(dataSnapshot.child("topictext").getValue().toString());
        mDatabase.child("users").child(dataSnapshot.child("uid").getValue().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userName.setText(dataSnapshot.child("username").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void loadComments(DataSnapshot dataSnapshot){
        listData.clear();
        try {
            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                String commenttext = postSnapshot.child("text").getValue().toString();
                String uid = postSnapshot.child("uid").getValue().toString();
                String username = postSnapshot.child("username").getValue().toString();


                listData.add(new CommentData(commenttext, uid, username));
            }

            if (!listData.isEmpty()) {
                SubViewAdpater adapter = new SubViewAdpater(listData, getContext());
                listView.setAdapter(adapter);
            }

        }catch(NullPointerException e){

        }

    }

    public interface SubViewClick{
        void subCommentClicked();
        void listViewClicked(CommentData data);

    }
    public void onStop(){
        super.onStop();
        if(eventListener != null)
            mDatabase.removeEventListener(eventListener);
    }
}
