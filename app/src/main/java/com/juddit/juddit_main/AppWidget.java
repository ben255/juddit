package com.juddit.juddit_main;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Implementation of App Widget functionality.
 */
public class AppWidget extends AppWidgetProvider {


    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.app_widget);
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.app_widget);

            //get the picked deadline
            SharedPreferences sp = context.getSharedPreferences("dates", Activity.MODE_PRIVATE);
            int year = sp.getInt("year", -1);
            int month = sp.getInt("month", -1);
            int day = sp.getInt("day", -1);

            //get current date
            Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            //only starts loading the progressbar when its within the month the deadline is at
            if(mYear == year && mMonth == month) {
                views.setProgressBar(R.id.widget_progressbar, day, mDay, false);
            }else
                views.setProgressBar(R.id.widget_progressbar, 1, 0, false);


            //if no date is chosen -1 is returned
            if(year == -1) {
                views.setTextViewText(R.id.widget_textView, context.getResources().getString(R.string.widget_text));
            }else {
                views.setTextViewText(R.id.widget_textView, "Deadline: "+year +":"+month+":"+day);
            }
            views.setTextViewText(R.id.widget_current_date, context.getResources().getString(R.string.current_date)+": "+mYear+":"+mMonth+":"+mDay);


            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

