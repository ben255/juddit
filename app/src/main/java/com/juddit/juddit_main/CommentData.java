package com.juddit.juddit_main;

/*
* a class to have a easier way saving data to the database
* */
public class CommentData {

    String text;
    String uid;
    String username;

    public CommentData(String text, String uid, String username){
        this.text = text;
        this.uid = uid;
        this.username = username;
    }

    public String getText(){
        return this.text;
    }
    public String getUid(){
        return this.uid;
    }
    public String getUsername(){
        return this.username;
    }
    public void setUsername(String username){this.username = username;}
}
