package com.juddit.juddit_main;

/*
 * a class to have a easier way saving data to the database
 * */

public class TopicData{
    public String room;
    public String title;
    public String topictext;
    public String uid;

    public TopicData(String room, String title, String topictext, String uid) {
        this.room = room;
        this.title = title;
        this.topictext = topictext;
        this.uid = uid;

    }
}
