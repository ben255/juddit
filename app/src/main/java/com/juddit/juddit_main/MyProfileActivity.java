package com.juddit.juddit_main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/*
* This activity loads the userdata, displays the chosen image or you can add one if you want too.
* also there is a option to go to settings.
* */

public class MyProfileActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private ImageView imageView, toolbarImage;
    private TextView username, email;
    private FirebaseAuth mAuth;
    private String uid;
    private static int RESULT_LOAD_IMAGE = 1;
    private TextView headerText;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.activity_myprofile);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        toolbarImage = (ImageView) findViewById(R.id.header_imageButton);
        headerText = (TextView) findViewById(R.id.header_textView);
        headerText.setText(getResources().getString(R.string.header_myprofile));
        setSupportActionBar(myToolbar);
        username = (TextView) findViewById(R.id.other_username_edittext);
        email = (TextView) findViewById(R.id.other_email_edittext);
        imageView = (ImageView) findViewById(R.id.other_image_view);


        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        uid = mAuth.getUid();
        mDatabase.child("users").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                loadData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

// Create a reference with an initial file path and name
        StorageReference pathReference = storageRef.child(uid);

        try {
            final File localFile = File.createTempFile("Images", "bmp");
            pathReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener< FileDownloadTask.TaskSnapshot >() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                   final Bitmap my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                   imageView.setImageBitmap(my_image);
                   toolbarImage.setImageBitmap(my_image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void createTopicRoomOnClick(View view) {
        Intent myIntent = new Intent(MyProfileActivity.this, TopicRoomActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        MyProfileActivity.this.startActivity(myIntent);
    }
    public void createMyProfileOnClick(View view){
    }

    public void loadData(DataSnapshot dataSnapshot){
        username.setText(dataSnapshot.child("username").getValue().toString());
        email.setText(dataSnapshot.child("email").getValue().toString());
    }

    public void settingsClick(View view){
        Intent myIntent = new Intent(MyProfileActivity.this, ProfileSettingsActivity.class);
        MyProfileActivity.this.startActivity(myIntent);
    }
    public void imageClick(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
    }

    public void deadlineClick(View view){
        Intent myIntent = new Intent(MyProfileActivity.this, ChoseDateActivity.class);
        MyProfileActivity.this.startActivity(myIntent);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

                FirebaseStorage storage = FirebaseStorage.getInstance();
                // Create a storage reference from our app
                StorageReference storageRef = storage.getReference();

                // Create a reference to "mountains.jpg"
                uid = mAuth.getUid();
                StorageReference mountainsRef = storageRef.child(uid);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] bitmapData = baos.toByteArray();

                UploadTask uploadTask = mountainsRef.putBytes(bitmapData);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(MyProfileActivity.this, R.string.went_wrong, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(MyProfileActivity.this, R.string.pick_image,Toast.LENGTH_LONG).show();
        }
    }

}
